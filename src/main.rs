use clap::{App, Arg, ArgMatches, SubCommand};
use rand::rngs::SmallRng;
use rand::{thread_rng, RngCore, SeedableRng};
use std::cmp::{max, min};
use std::fs::File;
use std::io::{Read, Seek, SeekFrom, Write};
use std::str::FromStr;
use memmap::MmapOptions;

fn main() {
    let matches = App::new("fscachebench")
        .version("0.1")
        .subcommand(
            SubCommand::with_name("readtest")
                .about("Test concurrent FS cache read performance")
                .arg(
                    Arg::with_name("threads")
                        .short("t")
                        .long("threads")
                        .takes_value(true)
                        .required(false)
                        .help("number of concurrent readers (threads)"),
                )
                .arg(
                    Arg::with_name("filesize")
                        .short("s")
                        .long("size")
                        .takes_value(true)
                        .required(false)
                        .help("Size of the generated test file (per reader), in bytes"),
                )
                .arg(
                    Arg::with_name("readsize")
                        .short("r")
                        .long("readsize")
                        .takes_value(true)
                        .required(false)
                        .help("Size of single 'read()' call, in bytes"),
                )
                .arg(
                    Arg::with_name("readtotal")
                        .short("n")
                        .long("readtotal")
                        .takes_value(true)
                        .required(false)
                        .help("Total 'read()' size, in bytes (may be larger than the file size)"),
                )
                .arg(
                    Arg::with_name("PATH")
                        .takes_value(true)
                        .required(true)
                        .index(1)
                        .help("Path to a temp directory for the generated test files"),
                ),
        )
        .subcommand(
            SubCommand::with_name("mmaptest")
                .about("Test concurrent FS cache read performance through mmap")
                .arg(
                    Arg::with_name("threads")
                        .short("t")
                        .long("threads")
                        .takes_value(true)
                        .required(false)
                        .help("number of concurrent readers (threads)"),
                )
                .arg(
                    Arg::with_name("filesize")
                        .short("s")
                        .long("size")
                        .takes_value(true)
                        .required(false)
                        .help("Size of the generated test file (per reader), in bytes"),
                )
                .arg(
                    Arg::with_name("readtotal")
                        .short("n")
                        .long("readtotal")
                        .takes_value(true)
                        .required(false)
                        .help("Total 'read()' size, in bytes (may be larger than the file size)"),
                )
                .arg(
                    Arg::with_name("PATH")
                        .takes_value(true)
                        .required(true)
                        .index(1)
                        .help("Path to a temp directory for the generated test files"),
                ),
        )
        .get_matches();

    match matches.subcommand() {
        ("readtest", Some(args)) => {
            println!("Running readtest");
            run_readtest(args);
        }
        ("mmaptest", Some(args)) => {
            println!("Running mmaptest");
            run_mmaptest(args);
        }
        _ => {
            println!("Please specify a sub-command");
            println!("{}", matches.usage());
        }
    }
}

#[derive(Debug)]
struct ReadStats {
    dummy_res: u64,
    read_bytes: usize,
}

fn run_readtest(args: &ArgMatches) {
    let path = args.value_of("PATH").unwrap();
    if !std::path::Path::new(path).exists() {
        println!("Path does not exist: '{}'", path);
        return;
    }

    let file_size = usize::from_str(args.value_of("filesize").unwrap_or("1048576")).unwrap();
    let file_size = max(file_size, 1024 * 1024);
    let read_size = usize::from_str(args.value_of("readsize").unwrap_or("65536")).unwrap();
    let read_size = min(file_size, max(read_size, 32));
    let read_total = usize::from_str(args.value_of("readtotal").unwrap_or("0")).unwrap();
    let read_total = max(read_total, file_size);
    let threads = usize::from_str(args.value_of("threads").unwrap_or("1")).unwrap();

    println!(
        "Read test params: file size = {}, single read size = {}, total read = {}, threads = {}",
        file_size, read_size, read_total, threads
    );
    println!("    temp path: '{}'", path);

    println!("Populating files...");
    let mut files = (0..threads)
        .map(|_| populate_file(path, file_size))
        .collect::<Vec<_>>();
    println!("Done populating");

    println!("Warming files...");
    files.iter_mut().for_each(|f| warm_file(f));
    println!("Done warming");

    let mut sw = stopwatch::Stopwatch::start_new();
    let hs = files
        .into_iter()
        .map(|file| {
            std::thread::spawn(move || {
                let mut file = file;
                let mut sw = stopwatch::Stopwatch::start_new();
                let stats = read_file(&mut file, read_size, read_total);
                sw.stop();
                println!(
                    "Per-file bandwidth: {:.2} GB/s",
                    (stats.read_bytes as f64) / (sw.elapsed_ms() as f64 / 1000.0) / 1024.0 / 1024.0 / 1024.0
                );
                stats
            })
        })
        .collect::<Vec<_>>();

    let stats_vec = hs
        .into_iter()
        .map(|h| h.join().unwrap())
        .collect::<Vec<_>>();
    sw.stop();

    let total_bytes = stats_vec.iter().map(|s| s.read_bytes).sum::<usize>();
    let res = stats_vec.iter().map(|s| s.dummy_res).sum::<u64>();
    println!(
        "Aggregate bandwidth: {:.2} GB/s",
        (total_bytes as f64) / (sw.elapsed_ms() as f64 / 1000.0) / 1024.0 / 1024.0 / 1024.0
    );
    println!("(res: {})", res);
}

fn read_file(file: &mut File, read_size: usize, read_total: usize) -> ReadStats {
    let file_size = file.metadata().unwrap().len() as usize;

    let mut buf = vec![0u8; read_size];
    let mut state = 0u64;
    let mut read_bytes = 0;

    while read_bytes < read_total {
        file.seek(SeekFrom::Start(0)).unwrap();
        let mut pos = 0;

        while pos < file_size && read_bytes < read_total {
            let s = min(file_size - pos, read_size);
            file.read_exact(&mut buf[..s]).unwrap();
            consume_buffer(&buf[..s], &mut state);
            pos += s;
            read_bytes += s;
        }
    }

    ReadStats {
        dummy_res: state,
        read_bytes,
    }
}

fn run_mmaptest(args: &ArgMatches) {
    let path = args.value_of("PATH").unwrap();
    if !std::path::Path::new(path).exists() {
        println!("Path does not exist: '{}'", path);
        return;
    }

    let file_size = usize::from_str(args.value_of("filesize").unwrap_or("1048576")).unwrap();
    let file_size = max(file_size, 1024 * 1024);
    let read_total = usize::from_str(args.value_of("readtotal").unwrap_or("0")).unwrap();
    let read_total = max(read_total, file_size);
    let threads = usize::from_str(args.value_of("threads").unwrap_or("1")).unwrap();

    println!(
        "Read test params: file size = {}, total read = {}, threads = {}",
        file_size, read_total, threads
    );
    println!("    temp path: '{}'", path);

    println!("Populating files...");
    let mut files = (0..threads)
        .map(|_| populate_file(path, file_size))
        .collect::<Vec<_>>();
    println!("Done populating");

    println!("Warming files...");
    files.iter_mut().for_each(|f| warm_file(f));
    println!("Done warming");

    let mut sw = stopwatch::Stopwatch::start_new();
    let hs = files
        .into_iter()
        .map(|file| {
            std::thread::spawn(move || {
                let mut file = file;
                let mut sw = stopwatch::Stopwatch::start_new();
                let stats = map_and_read_file(&mut file, read_total);
                sw.stop();
                println!(
                    "Per-file bandwidth: {:.2} GB/s",
                    (stats.read_bytes as f64) / (sw.elapsed_ms() as f64 / 1000.0) / 1024.0 / 1024.0 / 1024.0
                );
                stats
            })
        })
        .collect::<Vec<_>>();

    let stats_vec = hs
        .into_iter()
        .map(|h| h.join().unwrap())
        .collect::<Vec<_>>();
    sw.stop();

    let total_bytes = stats_vec.iter().map(|s| s.read_bytes).sum::<usize>();
    let res = stats_vec.iter().map(|s| s.dummy_res).sum::<u64>();
    println!(
        "Aggregate bandwidth: {:.2} GB/s",
        (total_bytes as f64) / (sw.elapsed_ms() as f64 / 1000.0) / 1024.0 / 1024.0 / 1024.0
    );
    println!("(res: {})", res);
}

fn map_and_read_file(file: &mut File, read_total: usize) -> ReadStats {
    let file_size = file.metadata().unwrap().len() as usize;

    let mut state = 0u64;
    let mut read_bytes = 0;

    while read_bytes < read_total {
        let mmap = unsafe {
            MmapOptions::new().len(file_size).map(file)
        }.unwrap();

        let chunk_size = min(read_total - read_bytes, file_size);
        consume_buffer(&mmap[..chunk_size], &mut state);
        read_bytes += chunk_size;
    }

    ReadStats {
        dummy_res: state,
        read_bytes,
    }
}

fn populate_file(path: &str, size: usize) -> File {
    let mut rng = SmallRng::from_rng(thread_rng()).unwrap();
    let mut buf = [0u8; 64 * 1024];
    let mut file = tempfile::tempfile_in(path).unwrap();
    let mut pos = 0;
    while pos < size {
        let to_write = min(size - pos, buf.len());
        rng.fill_bytes(&mut buf[..to_write]);
        file.write_all(&buf[..to_write]).unwrap();
        pos += to_write;
    }
    file.seek(SeekFrom::Start(0)).unwrap();
    file
}

fn consume_buffer(buf: &[u8], state: &mut u64) {
    let p = buf.as_ptr() as *const u64;
    let len = buf.len() / 8;
    for i in 0..len {
        unsafe {
            *state ^= *p.add(i);
        }
    }
}

fn warm_file(file: &mut File) {
    for _ in 0..2 {
        warm_file_once(file);
    }
}

fn warm_file_once(file: &mut File) {
    let read_size = 65536;
    let file_size = file.metadata().unwrap().len() as usize;
    file.seek(SeekFrom::Start(0)).unwrap();

    let mut buf = vec![0u8; read_size];
    let mut pos = 0;

    while pos < file_size {
        let s = min(file_size - pos, read_size);
        file.read_exact(&mut buf[..s]).unwrap();
        pos += s;
    }
}
